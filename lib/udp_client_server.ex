defmodule UdpClientServer do
  @moduledoc """
  Documentation for UdpClientServer.
  """

  @default_server_port 8005
  @local_host {192, 168, 43, 94}

  def launch_server do
    launch_server(@default_server_port)
  end

  def launch_server(port) do
    IO.puts "Launching server on localhost on port #{port}"
    server = Socket.UDP.open!(port)
    serve(server, [])
  end

  def serve(server, list) do
    {data, client} = server
                     |> Socket.Datagram.recv!

    IO.puts "Received: #{data}, from #{inspect(client)}"
    IO.inspect(Enum.member?(list, client))

    list = if Enum.member?(list, client) == false do
      list ++ [client]
    else
      list
    end

    temp = list -- [client]
    Enum.each(temp, fn (client) -> sent_to_any_client(server, data, client)  end)
    IO.inspect(list)
#    :timer.sleep(3000)
    serve(server, list)
  end

  def sent_to_any_client(server, data, client) do
    server
    |> Socket.Datagram.send!(data, client)
  end


  @doc """
  Sends `data` to the `to` value, where `to` is a tuple of
  { host, port } like {{127, 0, 0, 1}, 1337}
  """
  def send_data(data, to) do
    server = Socket.UDP.open!  # Without specifying the port, we randomize it
    Socket.Datagram.send!(server, data, to)
    {data, client} = server
                     |> Socket.Datagram.recv!
    IO.inspect(data)
  end

  def send_loop(to) do
    server = Socket.UDP.open!  # Without specifying the port, we randomize it
    my_send(server, to)

  end

  def my_send(server, to) do
    data = IO.gets("data: ")
           |> String.trim_trailing
    Socket.Datagram.send!(server, data, to)
    my_send(server, to)
  end

  def send_data(data) do
    send_data(data, {@local_host, @default_server_port})
  end
end
